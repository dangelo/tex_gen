use std::prelude::v1::String;
use std::prelude::v1::Vec;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

// Begin{block_type}{par1}{par2}


pub struct CmdBuilder {
    tag_type: TagType,
    block_name: String,
    par1: String,
}


impl CmdBuilder {
    pub fn new() -> CmdBuilder {
        CmdBuilder { tag_type: TagType::Begin, block_name: "".to_owned(), par1: "".to_string() }
    }

    pub fn tag_type(&mut self, tag_type: TagType) -> &mut CmdBuilder {
        self.tag_type = tag_type;
        self
    }

    pub fn name(&mut self, name: String) -> &mut CmdBuilder {
        self.block_name = name;
        self
    }

    pub fn par1(&mut self, par1: String) -> &mut CmdBuilder{
        self.par1 = par1;
        self
    }

    pub fn build(&self) -> String {
        let mut out: Vec<String> = vec![];

        out.push(format!("\\{}{{{}}}", self.tag_type.as_str(), self.block_name));

        if self.par1 != ""{
            out.push(format!("{{{}}}{{{}}}", self.par1, self.par1));
        }

        out.push("\n".to_owned());
        out.join("")
    }
}

pub enum TagType {
    Begin,
    End,
}

impl TagType{
    pub fn as_str(&self) -> &str {
//    pub fn as_str(&self) -> &'static str { // TODO why 'static str
        match self {
            TagType::Begin => "begin",
            TagType::End => "end",
        }
    }
}

#[test]
fn tag_builder_begin_test(){
    let begin_tag = CmdBuilder::new()
        .tag_type(TagType::Begin)
        .name("test".to_owned())
        .build();
    assert_eq!("\\begin{test}\n", begin_tag)
}

#[test]
fn tag_builder_begin_par_test(){
    let begin_tag = CmdBuilder::new()
        .tag_type(TagType::Begin)
        .name("test".to_owned())
        .par1("ti".to_owned())
        .build();
    assert_eq!("\\begin{test}{ti}{ti}\n", begin_tag)
}

#[test]
fn tag_builder_end_test(){
    let end_tag = CmdBuilder::new()
        .tag_type(TagType::End)
        .name("test".to_owned())
        .build();
    assert_eq!("\\end{test}\n", end_tag)
}
